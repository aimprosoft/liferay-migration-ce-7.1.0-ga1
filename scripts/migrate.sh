#!/usr/bin/env bash

######## Custom Methods ################

process_command()
{
  if [ $? -eq 0 ]
  then
    echo $1
  else
    echo $2
    exit 1
  fi
}

######## Properties ################

#MySQL
db_user=root
db_password=1

#LR 6.2
lr62_path=/home/vitaliy/Work/Liferay/migration/liferay-6.2-ce-ga4/liferay-portal-6.2-ce-ga4
lr62_db_name=lr_migration

#LR 7.1
lr71_path=/home/vitaliy/Work/Liferay/migration/liferay-ce-7.1.0-ga1
lr71_archive=liferay-ce-portal-tomcat-7.1.0-ga1-20180703012531655.zip
lr71_dir=liferay-ce-portal-7.1.0-ga1
lr71_db_name=lr_migration_dxp

echo "[$(date -u +%H:%M:%S)] ========== START Database migration ============== "


#Step 1 - install Liferay 7
echo "[$(date -u +%H:%M:%S)] Installing Liferay 7.1..."
cd ${lr71_path}
rm -Rf ${lr71_dir}
unzip ${lr71_archive}
process_command "[$(date -u +%H:%M:%S)] Liferay 7.1 installed successfully." "[$(date -u +%H:%M:%S)] Failed to install Liferay 7.1."



#Step 2 - create dump for 6.2
echo "[$(date -u +%H:%M:%S)] Creating DB dump for 6.2 database..."
rm -Rf ${lr62_db_name}.sql
mysqldump -u${db_user} -p${db_password} --routines ${lr62_db_name} > ${lr62_db_name}.sql
process_command "[$(date -u +%H:%M:%S)] DB dump created successfully." "[$(date -u +%H:%M:%S)] Failed to create DB dump."



#Step 3 - load dump to 7.1 database
echo "[$(date -u +%H:%M:%S)] Creating database for 7.1 and loading dump..."
mysql -u${db_user} -p${db_password} -e "drop database if exists ${lr71_db_name}"
mysql -u${db_user} -p${db_password} -e "create database ${lr71_db_name} character set utf8"
mysql -u${db_user} -p${db_password} ${lr71_db_name} < ${lr62_db_name}.sql
process_command "[$(date -u +%H:%M:%S)] DB dump loaded successfully." "[$(date -u +%H:%M:%S)] Failed to load DB dump."



#Step 4 - configuring portal-setup-wizard.properties
echo "[$(date -u +%H:%M:%S)] Creating portal-setup-wizard.properties file..."
cd ${lr71_path}/${lr71_dir}
rm -Rf portal-setup-wizard.properties
touch portal-setup-wizard.properties
echo "admin.email.from.address=test@liferay.com" >> portal-setup-wizard.properties
echo "admin.email.from.name=Test Test" >> portal-setup-wizard.properties
echo "liferay.home=${lr71_path}/${lr71_dir}" >> portal-setup-wizard.properties
echo "setup.wizard.enabled=false" >> portal-setup-wizard.properties
echo "jdbc.default.driverClassName=com.mysql.jdbc.Driver" >> portal-setup-wizard.properties
echo "jdbc.default.url=jdbc:mysql://localhost:3306/${lr71_db_name}?useUnicode=true&characterEncoding=UTF-8&useFastDateParsing=false" >> portal-setup-wizard.properties
echo "jdbc.default.username=${db_user}" >> portal-setup-wizard.properties
echo "jdbc.default.password=${db_password}" >> portal-setup-wizard.properties
process_command "[$(date -u +%H:%M:%S)] portal-setup-wizard.properties created successfully." "[$(date -u +%H:%M:%S)] Failed to create portal-setup-wizard.properties."



#Step 5 - copying data
echo "[$(date -u +%H:%M:%S)] Copying data..."
cd ${lr71_path}/${lr71_dir}
cd data
cp -R ${lr62_path}/data/document_library .
process_command "[$(date -u +%H:%M:%S)] Data copied successfully..." "[$(date -u +%H:%M:%S)] Failed to copy data."




#Step 6 - disabling indexer
echo "[$(date -u +%H:%M:%S)] Disabling indexer..."
cd ${lr71_path}/${lr71_dir}/osgi/configs
touch com.liferay.portal.search.configuration.IndexStatusManagerConfiguration.config
echo "indexReadOnly=\"true\"" >> com.liferay.portal.search.configuration.IndexStatusManagerConfiguration.config
process_command "[$(date -u +%H:%M:%S)] Indexer disabled successfully..." "[$(date -u +%H:%M:%S)] Failed to disable indexer."



#Step 7 - configuring upgrade tool
echo "[$(date -u +%H:%M:%S)] Configuring upgrate tool..."

cd ${lr71_path}/${lr71_dir}/tools/portal-tools-db-upgrade-client

echo "dir=../../tomcat-9.0.6" > app-server.properties
echo "extra.lib.dirs=/bin" >> app-server.properties
echo "global.lib.dir=/lib" >> app-server.properties
echo "portal.dir=/webapps/ROOT" >> app-server.properties
echo "server.detector.server.id=tomcat" >> app-server.properties

echo "jdbc.default.driverClassName=com.mysql.jdbc.Driver" > portal-upgrade-database.properties
echo "jdbc.default.url=jdbc:mysql://localhost:3306/${lr71_db_name}?useUnicode=true&characterEncoding=UTF-8&useFastDateParsing=false" >> portal-upgrade-database.properties
echo "jdbc.default.username=${db_user}" >> portal-upgrade-database.properties
echo "jdbc.default.password=${db_password}" >> portal-upgrade-database.properties

echo "liferay.home=../../" > portal-upgrade-ext.properties
echo "module.framework.base.dir=../../osgi" >> portal-upgrade-ext.properties

process_command "[$(date -u +%H:%M:%S)] Upgrade tool configured successfully..." "[$(date -u +%H:%M:%S)] Failed to configured upgrade tool."




#Step 8 - running updgrade tool
echo "[$(date -u +%H:%M:%S)] Running upgrate tool..."
cd ${lr71_path}/${lr71_dir}/tools/portal-tools-db-upgrade-client
chmod +x db_upgrade.sh
./db_upgrade.sh -l "db_upgrade.log"
process_command "[$(date -u +%H:%M:%S)] Upgrade tool finished successfully..." "[$(date -u +%H:%M:%S)] Failed to run upgrade tool."


echo "[$(date -u +%H:%M:%S)] ========== Database migration COMPLETED ========== "